#[allow(clippy::print_literal)]
fn main() {
    print!("print to stdout without newline");
    println!("print to stdout with newline");
    eprint!("print to stderr without newline");
    eprintln!("print to stderr with newline");

    println!("my parameter is {}", "foo");
    println!("{0} {1} {1} {0}", "foo", "bar");
    println!(
        "{title}: {author} ({year})",
        title = "Anna Karenina",
        author = "Leo Tolstoy",
        year = "1877"
    );

    let s_format: String = format!("print to string with parameter: {}", "foo");
    println!("{}", s_format);

    let s_concat: &str = concat!("one", "very", "long", "string");
    println!("{}", s_concat);
}
