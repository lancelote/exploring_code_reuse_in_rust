macro_rules! my_vec {
    ( $( $x:expr ),* ) => {
        {
            let mut temp_vec = Vec::new();
            $(
                temp_vec.push($x);
            )*
            temp_vec
        }
    };
}

fn main() {
    let my_vec = my_vec!(1, 2, 3);
    assert_eq!(my_vec, vec![1, 2, 3]);
}
