macro_rules! hello {
    () => {
        // match no arguments
        println!("hello");
    };
}

fn main() {
    hello!();
}
