fn main() {
    let path: &'static str = env!("PATH"); // evaluated at compile time
    println!("$PATH is {}", path);

    let key: Option<&'static str> = option_env!("SECRET_KEY");
    println!("the secret key is {:?}", key);
}
