#[allow(clippy::no_effect)]
fn main() {
    if cfg!(target_os = "macos") {
        println!("run the macOS-specific code");
        MacOSCode {};
    } else if cfg!(target_os = "linux") {
        println!("run the linux-specific code");
    } else {
        println!("unsupported OS");
    }
}

struct MacOSCode;
