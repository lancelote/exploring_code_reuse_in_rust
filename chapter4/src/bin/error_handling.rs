use std::fs::File;
use std::io;
use std::io::prelude::*;

pub fn write_to_file_try() -> Result<(), io::Error> {
    // File::create() -> Result<File>
    File::create("my_file.txt")?.write_all(b"hello world")
}

// equivalent to
pub fn write_to_file_match() -> Result<(), io::Error> {
    match File::create("my_file.txt") {
        Ok(mut f) => f.write_all(b"hello world"),
        Err(e) => Err(e),
    }
}

fn main() {
    println!("{:?}", write_to_file_try());
}
