# exploring_code_reuse_in_rust

Code from "Exploring code reuse in Rust" video course by Shing Lyu

## TOC

- [x] Chapter 1: Basics of Code Reuse
    - [Iterators and Ownership](chapter1/src/bin/playing_with_iterators.rs)
    - [Iterator Adapter](chapter1/src/bin/iterator_adapter.rs)
    - [Using "for each"](chapter1/src/bin/using_for_each.rs)
- [x] Chapter 2: Using Generics in Rust
    - [Generics Basics](chapter2/src/bin/generics_basics.rs)
    - [Generics in Structs](chapter2/src/bin/generics_in_structs.rs)
    - [Generics in "impl" Blocks](chapter2/src/bin/generics_impl.rs)
- [x] Chapter 3: Defining Interfaces with Traits
    - [Traits Basics](chapter3/src/bin/traits_basics.rs)
    - [Trait Bounds in "where" block](chapter3/src/bin/bounds_in_where.rs)
    - [Trait Objects](chapter3/src/bin/duck_typing_with_trait.rs)
    - [Traits with Generics](chapter3/src/bin/trait_with_generics.rs)
    - [Associated Types](chapter3/src/bin/associated_types.rs)
    - [Dynamic Dispatch](chapter3/src/bin/dynamic_dispatch.rs)
    - [Operator Overloading](chapter3/src/bin/operator_overloading.rs)
    - [Add Trait](chapter3/src/bin/add_trait.rs)
    - [Drop Trait](chapter3/src/bin/drop_trait.rs)
    - ["FnOne" Trait](chapter3/src/bin/fn_once_traits.rs)
    - ["From" Trait](chapter3/src/bin/from_trait.rs)
    - ["Copy" Marker Trait](chapter3/src/bin/copy_marker_trait.rs)
    - ["Display" Trait](chapter3/src/bin/display_trait.rs)
    - ["Default" Trait"](chapter3/src/bin/default_trait.rs)
    - ["Default" Trait for Enums](chapter3/src/bin/default_enum.rs)
- [x] Chapter 4: Hacking the language with Macros and Compiler Plugins
    - [Basic Macro](chapter4/src/bin/basic_macro.rs)
    - [Custom "vec!" Macro](chapter4/src/bin/custom_vec.rs)
    - ["vec!" Macro](chapter4/src/bin/vec_macro.rs)
    - ["env!" Macro](chapter4/src/bin/env_macro.rs)
    - [Formatting Macros](chapter4/src/bin/formatting_macros.rs)
    - [File Position Macros](chapter4/src/bin/file_position_macro.rs)
    - [Conditional Compiling](chapter4/src/bin/conditional_compiling.rs)
    - [Error Handling](chapter4/src/bin/error_handling.rs)
- [x] Chapter 5: Reusing the Code with Other People Using Modules and Creates
