#[allow(unused_variables)]
struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn get_x(&self) -> &T {
        &self.x
    }

    fn get_y(&self) -> &T {
        &self.y
    }
}

fn main() {
    let p = Point { x: 5, y: 10 };
    println!("({}, {})", p.get_x(), p.get_y());

    let p = Point { x: 5.0, y: 10.0 };
    println!("({}, {})", p.get_x(), p.get_y());
}
