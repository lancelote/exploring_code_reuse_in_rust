trait Iterator<T> {
    fn next(&mut self) -> Option<T>;
}

struct Counter;

impl Iterator<u32> for Counter {
    fn next(&mut self) -> Option<u32> {
        Some(42)
    }
}

impl Iterator<String> for Counter {
    fn next(&mut self) -> Option<String> {
        Some(String::from("hello world"))
    }
}

fn main() {
    let mut counter = Counter {};

    let n_int: Option<u32> = counter.next();
    println!("{:?}", n_int);

    let n_string: Option<String> = counter.next();
    println!("{:?}", n_string);
}
