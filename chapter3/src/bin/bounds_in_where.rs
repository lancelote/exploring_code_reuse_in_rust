fn largest<T>(list: &[T]) -> T
where
    T: PartialOrd + Copy,
{
    let mut largest = list[0];
    //                    ^^^^^^^ Copy

    for &item in list {
        // ^^ Copy
        if item > largest {
            //  ^ PartialOrd
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![34, 50, 25, 100, 65];
    println!("largest: {}", largest(&number_list));
}
