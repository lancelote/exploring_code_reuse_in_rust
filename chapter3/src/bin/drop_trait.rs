use std::ops::Drop;

struct MyType;

impl Drop for MyType {
    fn drop(&mut self) {
        println!("dropping");
    }
}

fn main() {
    println!("declaring");
    let _ = MyType;
    println!("about to exit");
}
