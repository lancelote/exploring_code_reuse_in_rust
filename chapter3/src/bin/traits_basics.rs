trait Vehicle {
    fn new(name: &'static str) -> Self; // static method
    fn name(&self) -> &'static str;
    fn go(&self); // instance method
    fn to_string(&self) {
        // default implementation
        println!("vehicle {}", self.name());
    }
}

struct Airplane {
    name: &'static str,
}

struct Bicycle {
    name: &'static str,
}

struct Car {
    name: &'static str,
}

impl Vehicle for Airplane {
    fn new(name: &'static str) -> Self {
        Airplane { name }
    }

    fn name(&self) -> &'static str {
        self.name
    }

    fn go(&self) {
        self.fly();
    }
}

impl Vehicle for Bicycle {
    fn new(name: &'static str) -> Self {
        Bicycle { name }
    }

    fn name(&self) -> &'static str {
        self.name
    }

    fn go(&self) {
        self.pedal();
    }
}

impl Vehicle for Car {
    fn new(name: &'static str) -> Self {
        Car { name }
    }

    fn name(&self) -> &'static str {
        self.name
    }

    fn go(&self) {
        self.drive();
    }
}

impl Airplane {
    fn fly(&self) {
        println!("fly {}", self.name);
    }
}

impl Bicycle {
    fn pedal(&self) {
        println!("pedal {}", self.name);
    }
}

impl Car {
    fn drive(&self) {
        println!("drive {}", self.name);
    }
}

fn from_amsterdam_to_paris<T: Vehicle>(vehicle: T) {
    //                        ^^^^^^^ trait bound
    vehicle.go();
}

fn main() {
    let bicycle = Bicycle::new("MyBike");
    from_amsterdam_to_paris(bicycle);
}
