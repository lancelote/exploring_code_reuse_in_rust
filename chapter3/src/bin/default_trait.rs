#[derive(Default, Debug)]
struct MyData {
    int_field: i32,
    float_field: f32,
}

fn main() {
    // Use default values for all fields
    let data1: MyData = Default::default();
    println!("{:?}", data1);

    // Specify part of the fields
    let data2 = MyData {
        int_field: 42,
        ..Default::default() // Use default for the rest
    };
    println!("{:?}", data2);
}
