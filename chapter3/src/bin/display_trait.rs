use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
struct Cat {
    name: &'static str,
    breed: &'static str,
    age: u8,
}

impl Display for Cat {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(
            f,
            "{} the {}-years old {} cat",
            self.name, self.age, self.breed
        )
    }
}

fn main() {
    let simba = Cat {
        name: "Simba",
        breed: "Lion",
        age: 2,
    };

    println!("{}", simba);

    // Debug
    println!("{:?}", simba);

    // Debug pretty-print
    println!("{:#?}", simba);
}
