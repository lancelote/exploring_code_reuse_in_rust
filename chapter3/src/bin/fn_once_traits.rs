fn consuming_call<F>(func: F)
where
    F: FnOnce() -> String,
{
    println!("consumed: {}", func());
}

fn main() {
    let x = String::from("hello");

    // Closure that consumes x and returns it directly
    let return_x = move || x;

    // FnOnce is auto-implemented for closures which may consume captured variables
    consuming_call(return_x);
}
