#[derive(Debug)]
struct MyNumber {
    value: i32,
}

impl From<i32> for MyNumber {
    fn from(number: i32) -> Self {
        MyNumber { value: number }
    }
}

// With From implementation we get Into for free

fn main() {
    let num1 = MyNumber::from(42i32);
    let num2: MyNumber = 42i32.into();

    println!("num1: {:?}", num1);
    println!("num2: {:?}", num2);
}
