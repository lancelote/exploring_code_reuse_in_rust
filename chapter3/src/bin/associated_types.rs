use std::fmt;

trait Iterator {
    type Item: fmt::Display; // associated type

    fn next(&mut self) -> Option<Self::Item>;
}

struct Counter;

impl Iterator for Counter {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        Some(42)
    }
}

fn main() {
    let mut counter = Counter {};

    let n = counter.next();
    println!("{:?}", n);
}
