#[derive(Debug)]
struct MyStruct;

// Trivial bitwise copy
// Empty as it's a marker trait
impl Copy for MyStruct {}

// Explicit memory copy, usually more expensive
impl Clone for MyStruct {
    fn clone(&self) -> Self {
        *self
    }
}

// Usually Copy and Clone can be used as Debug for auto-implementation

#[allow(unused_variables)]
fn main() {
    let x = MyStruct {};
    let y = x; // copy, x was not moved
}
