fn owned() {
    println!("owned");
    let my_list = vec![1, 2, 3, 4];

    for element in my_list.into_iter() {
        println!("{}", element);
    }

    // println!("{}", my_list[0]);
    // Can't reuse moved value - iterator took ownership over vector values

    println!();
}

fn reference() {
    println!("reference");
    let my_list = vec![1, 2, 3, 4];

    for element in my_list.iter() {
        println!("{}", element)
    }

    println!("first: {}", my_list[0]);
    println!();
}

fn mut_reference() {
    let mut my_list = vec![1, 2, 3, 4];

    for element in my_list.iter_mut() {
        *element += 1;
        println!("{}", element);
    }

    println!("first: {}", my_list[0]);
}

fn main() {
    owned();
    reference();
    mut_reference();
}
